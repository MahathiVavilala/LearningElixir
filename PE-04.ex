defmodule PE04 do
  def main do
      Enum.map(tuples, fn({a,b}) -> a * b end)
        |> Enum.filter(&palindrome/1)
        |> Enum.max
    end

    def tuples do
      for x <- Enum.to_list(999..100), y <- Enum.to_list(999..100), do: {x, y}
    end

    defp palindrome(str) do
        Integer.to_string(str) == String.reverse(Integer.to_string(str))
    end
end
