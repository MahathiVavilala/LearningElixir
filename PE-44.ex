defmodule PE44 do
 def main do

  pentagonals = Enum.map(1..3000, fn(x) -> pentagonal(x) end)
  for j <- pentagonals, k <- pentagonals, k > j,
     pentagonal?(j + k) && pentagonal?(k - j),
     do:  {k, j, abs(k - j)}
  end

  def pentagonal(n), do: trunc(n * ((3 * n) - 1) / 2)

  def pentagonal?(n) do
    pen = ((:math.sqrt(24 * n + 1)) + 1) / 6
    pen == trunc(pen)
  end
end
