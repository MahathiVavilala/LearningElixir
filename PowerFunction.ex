defmodule Power do
  def main(n, m) do
    power(n,m)
  end
  def power(a, 1), do: a

  def power(a, n) when rem(n, 2) === 0 do
     pow = power(a, div(n, 2))
     pow * pow
  end

  def power(a, n) do
     a * power(a, n-1)
  end
end
