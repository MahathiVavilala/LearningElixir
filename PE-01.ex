defmodule Solution do
  range = 1..999
  sum = Enum.sum (Enum.filter(range, fn(x) -> rem(x, 3) == 0 or rem(x, 5) == 0 end))
  IO.puts sum
end
