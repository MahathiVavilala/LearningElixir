defmodule PE29 do
    def main do
        Enum.count(Enum.uniq(PE29.count))
    end
    def count do
        sequence =  for  a <- 2..100,
                         b <- 2..100,
                         do: (:math.pow(a, b))
    end
end
