defmodule UniqueInt do
  use Bitwise
  def main do
    [1,2,4,2,1]
    |>Enum.map_reduce( 0, fn(x, acc) -> {x , x ^^^ acc} end)
    |>Tuple.to_list
    |>List.last
  end
end

