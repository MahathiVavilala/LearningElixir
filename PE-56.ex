defmodule PE56 do
  def main do
    Enum.max(Enum.map(Enum.map((for a <- 60..100, b <- 80..100, do: power(a, b)), fn(x) -> Integer.digits(x) end), fn(x) -> Enum.sum(x) end))
  end
  def power(a, 1), do: a
  def power(a, n) when rem(n, 2) === 0 do
     pow = power(a, div(n, 2))
     pow * pow
  end
  def power(a, n) do
     a * power(a, n-1)
  end
end
