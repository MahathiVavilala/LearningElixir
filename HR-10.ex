defmodule HR10 do
  def sumOddElements(l) do
     Enum.sum(Enum.filter(l, fn(x) -> rem(x, 2) != 0 end))
  end
end
