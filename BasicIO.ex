defmodule BasicIO do
  def main do
    userName = IO.gets("HI! What's your name? ") |> String.trim
    IO.puts "Hello #{userName}"
  end
end
