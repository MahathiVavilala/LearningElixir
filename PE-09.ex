defmodule Triple do
  def pythagorean(n) when n > 0 do
    k = for a <- 1..300,
        b <- a..600,
        c <- b..1000,
        a + b + c == 1000,
        a*a + b*b == c*c,
        do:  (a* b* c)
    Enum.uniq(k)
  end
end
