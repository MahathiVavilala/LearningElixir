defmodule Solution do
    def filter(enum, fun) do
        Enum.reduce(enum, [], fn(x, acc) -> if fun.(x) do acc ++ [x] else acc end end)
    end
    def main do
        max = IO.read(:stdio, :line) |> String.trim |> String.to_integer
        IO.stream(:stdio, :line)
        |> Enum.map(&String.trim/1)
        |> Enum.map(&String.to_integer/1)
        |> Solution.filter(fn(x) -> x < max end)
        |> Enum.each(&IO.puts(&1))
    end
end
Solution.main()
