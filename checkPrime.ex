
defmodule Prime do
  def main(n) do
    if is_prime(n) do
      n
    end
  end
  def is_prime(x), do: (2..x |> Enum.filter(fn a -> rem(x, a) == 0 end) |> length()) == 1  
end
