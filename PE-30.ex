defmodule PeThirty do
  def fifthpowers(n) do
    for a <- 1..20,
     do: :Math.pow(a,n) |> round #=> 8
   end
 end
