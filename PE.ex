defmodule PE do
  def try do
    perms([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
     |> Enum.take(1_000_000)
     |> Enum.sort
     |> List.last
     |> Enum.join
  end
  def perms([]), do: [[]]
    def perms(list) do
      lc head inlist list, tail inlist perms(list -- [head]), do: [head|tail]
    end
end
