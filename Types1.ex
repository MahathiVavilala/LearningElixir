defmodule BasicTypes do
  def main do
    types()
  end

  def types do
    int = 111
    IO.puts "Integer #{is_integer(int)}"

    floating = 111.1234
    IO.puts "Float #{is_float(floating)}"

    atom = :"Hyderabad City"
    IO.puts "Atom #{is_atom(atom)}"
    #IO.puts "Atom #{is_atom(:"TalentSprint : Hyderabad City")}"

  end
end
