
defmodule PE48 do
  def main do

  k =  Enum.filter(1..1000,fn(n) -> rem(n, 10) != 0 end)
     |> Enum.map(fn(x) -> power(x,x) end)
     |>Enum.sum
     |>Integer.to_string
  String.slice(k, (String.length(k) - 10)..(String.length(k)))
  end
  def power(_, 0), do: 1
  def power(a, 1), do: a
  def power(a, n) when rem(n, 2) === 0 do
     pow = power(a, div(n, 2))
     pow * pow
  end
  def power(a, n) do
     a * power(a, n-1)
  end
end
