defmodule PE32 do
  def main() do
    list = makeTypes(Enum.map(permutations([1, 2, 3, 4, 5, 6, 7, 8, 9]), fn(c) -> Enum.join(c) end))
    Enum.map(list, fn(x) -> validExp(x) end)
  end

  def permutations([]), do: [[]]
  def permutations(list) do
    for head <- list, tail <- permutations(list -- [head]), do: ([head|tail])
  end

  def makeTypes(p) do
     typeA = Enum.map((List.zip([Enum.map(p, fn(v) -> String.to_integer(String.slice(v, 0,1)) end), Enum.map(p, fn(v) -> String.to_integer(String.slice(v, 1,4)) end), Enum.map(p, fn(v) -> String.to_integer(String.slice(v, 5,4)) end)])), fn(x) -> Tuple.to_list(x) end)
     typeB = Enum.map((List.zip([Enum.map(p, fn(v) -> String.to_integer(String.slice(v, 0,2)) end), Enum.map(p, fn(v) -> String.to_integer(String.slice(v, 2,3)) end), Enum.map(p, fn(v) -> String.to_integer(String.slice(v, 5,4)) end)])), fn(x) -> Tuple.to_list(x) end)
     typeA ++ typeB
  end

  def validExp(l) do
    initial = List.first(l)
    middle = List.first(l -- List.insert_at(List.wrap(List.last(l)), 0, List.first(l)))
    final = List.last(l)
    if (initial * middle == final) do
       [initial * final]
    end
  end
end

