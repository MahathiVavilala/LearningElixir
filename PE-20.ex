defmodule Factorial do
  def fact(num) when num > 0 do
     Enum.sum(Integer.digits(Enum.reduce 1..num, 1, &(&1 * &2)))
  end
end
