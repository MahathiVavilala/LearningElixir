defmodule PE06 do
  def main do
    squareSum((0..100)) - sumSquares((0..100))
  end

  def sumSquares(seq) do
    Enum.reduce(seq, 0, fn(x, acc) -> (x * x) + acc end)
  end

  def squareSum(seq) do
    sum = Enum.reduce(seq, 0, fn(x, acc) -> x + acc end)
    sum * sum
  end
end
